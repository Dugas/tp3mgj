'use strict';

var express = require('express');
// Router pour l'API REST.
var routerApi = express.Router();
// ORM Mongoose.
var mongoose = require('mongoose');

var uri = "mongodb://garneau:12345@tp3-shard-00-00-gpguv.mongodb.net:27017,tp3-shard-00-01-gpguv.mongodb.net:27017,tp3-shard-00-02-gpguv.mongodb.net:27017/test?ssl=true&replicaSet=tp3-shard-0&authSource=admin";
mongoose.connect(uri);
var MongoClient = require('mongodb').MongoClient;
// mongoose.connect('mongodb://localhost:27017/demo-pokemon', {useMongoClient: true, poolSize: 10});

// Modèle Mongoose pour les utilisateurs.
var UtilisateurModel = require('../models/UtilisateurModel').utilisateurModel;
var CollectionModel = require('../models/UtilisateurModel').collectionModel;
var PointsInteretModel = require('../models/UtilisateurModel').pointsInteretModel;

// Exécution lors de chaque requête faite à l'API.
routerApi.use(function (req, res, next) {
    // Log de chaque requête faite à l'API.
    console.log(req.method, req.url);
    // On pourrait valider le jeton d'accès ici !

    // Permet de poursuivre le traitement de la requête.
    next();
});


// Route désignant un certain Utilisateur
// ==================================
routerApi.route('/utilisateurs/:nom')

    // Consultation d'un certain utilisateur.
    .get(function (req, res) {
        console.log('Consultation de l\'utilisateur ' + req.params.nom);

        UtilisateurModel.findOne({'nom':req.params.nom}, function (err, utilisateur) {
            if (err) throw err;
            if (utilisateur)
                res.json(utilisateur);
            else
                res.status(204).end();
        });
    })

    // Création d'un utilisateur.
    // Une fois créé, on ne peut plus le modifier.
    .post(function (req, res) {

        var utilisateur = new UtilisateurModel();
        utilisateur.nom = req.body.nom;
        utilisateur.passe = req.body.passe;
        utilisateur.courriel = req.body.courriel;

        utilisateur.save(function(err){
            if(err)
                res.send(err);

            res.json({_id: utilisateur._id, nom: utilisateur.nom, passe: utilisateur.passe, courriel: utilisateur.courriel});
        });
    })

// Route désignant une Collection
// ==================================
routerApi.route('/utilisateurs/:nom/collections/:nomCollection')
    //Get d'une collection
    .get(function(req, res){
        var i;
        UtilisateurModel.findOne({'nom':req.params.nom}, function(err, util)
    {
        if(err) throw err;
        CollectionModel.findOne({'nom':req.params.nomCollection}, function(err, col)
        {
            if(err) throw err;
            if(col!=null)
            {
            for(i=0; i < util.lesCollections.length; i++)
            {
                if(util.lesCollections[i].nom==col.nom)
                {
                    res.status(200).json(util.lesCollections[i]);
                    break;
                }
            }
            }
            else{
                res.status(204).end();
            }
        });
    });
})

    .post(function(req, res){
        UtilisateurModel.findOne({'nom': req.params.nom}, function(err, utilisateur){
            if (err) throw err;
        
            console.log(utilisateur.nom);
            var collection = new CollectionModel(req.body);
            collection.save(function(err){
                if(err) throw err;
                // récupération du vexteur de collections.
                var lesCols = utilisateur.lesCollections;
                lesCols.push(collection);
                //assignation du vecteur mis à jour à la collection
                utilisateur.lesCollections = lesCols;
                //sauvegarde dans la BD
                utilisateur.save(function(err){
                    if(err) throw err;
                });

            res.status(201).json(collection);
        });
    });
    })
    
    .put(function(req,res){
        //Modification
        var i;
        var collection= new CollectionModel();
        collection.nom=req.body.nom;

        UtilisateurModel.findOne({'nom': req.params.nom}, function(err, utilisateur){
            if (err) throw err;
        
            CollectionModel.findOne({'nom':req.params.nomCollection},function(err,col)
            {
                if(err) throw err;
                if(col!=null)
                {
                  for(i=0; i < utilisateur.lesCollections.length; i++)
                    {
                        if(utilisateur.lesCollections[i].nom==col.nom)
                        {
                            collection.lesPointsInteret=utilisateur.lesCollections[i].lesPointsInteret;
                            utilisateur.lesCollections[i]=collection;
                            utilisateur.save(function(err){
                                if(err) throw err;
                            })
                            res.status(200).json(collection);
                            break;
                        }
                    }
                }
                else{
                    res.status(204).end();
                }
        });
    });
})

    .delete(function(req, res){
        console.log('Destruction d\'une collection');
        var i;
        UtilisateurModel.findOne({'nom':req.params.nom}, function(err, utilisateur){
            if(err) throw err;
            CollectionModel.findOne({'nom':req.params.nomCollection}, function(err, col){
                if (err) throw err;

                if(col!=null){
                    for(i=0; i < utilisateur.lesCollections.length; i++)
                    {
                        if(utilisateur.lesCollections[i].nom==col.nom)
                        {
                            utilisateur.lesCollections.splice(i,1);
                            utilisateur.save();
                            res.status(204).end();
                            break;
                        }
                    }
                }
                else{
                    res.json({message:'La collection n\'existe pas'});
                }

            });
        });
        
    })
    // Méthode HTTP non permise
    .all(function (req, res) {
        console.log('Méthode HTTP non permise.');
        res.status(405).end();
    });

// Route désignant l'ensemble de tous les collections d'un utilisateur
// ===================================================================
routerApi.route('/utilisateurs/:nom/collections')
    .get(function(req, res){
        UtilisateurModel.findOne({'nom':req.params.nom}, function(err, util){
            if(err) throw err;
            if(util!=null)
            {
                res.status(200).json(util.lesCollections);
            }
            else{
                res.status(204).end();
            }
        });
    })

    .delete(function(req, res){
        console.log('Destruction des collection');
        UtilisateurModel.findOne({'nom':req.params.nom}, function(err, util){
            if(err) throw err;
            util.lesCollections = [];
            util.save();
            res.status(204).end();
        });
    })

// Route désignant les points d'intérêt 
// ==========================================
routerApi.route('/utilisateurs/:nom/collections/:nomCollection/pointsinteret')
//modification d'un certain point d'intérêt 
    .post(function (req, res) {
        console.log('Création d\'un nouveau  point d\'intérêt dans la collection ' + req.params.nomCollection);
        var i;
        var pointsInteret = new PointsInteretModel(req.body);
        pointsInteret.save(function (err) {                    
            if (err) throw err;
        
            UtilisateurModel.findOne({'nom':req.params.nom}, function(err, utilisateur){
            //récupération de la collection dans laquelle insérer le nouveau point d'intérêt
                CollectionModel.findOne({'nom':req.params.nomCollection}, function (err, Collection) {
                    if (err) throw err;
                    //récupération du vecteur de points d'intérêt
                    for(i=0; i < utilisateur.lesCollections.length; i++)
                    {
                        if(utilisateur.lesCollections[i].nom==Collection.nom)
                        {
                            var lespts = utilisateur.lesCollections[i].lesPointsInteret;
                            lespts.push(pointsInteret);
                            //assignation du vecteur mis à jour à la collection
                            utilisateur.lesCollections[i].lesPointsInteret = lespts;
                            //sauvegarde dans la BD
                            utilisateur.save(function (err) {                    
                            if (err) throw err;
                            });
                            break;
                        }
                    }
                });
                res.status(201).json(pointsInteret);
            });
        });
    })

    // Destruction d'un certain point d'intérêt.
    .delete(function (req, res) {
    console.log('Destruction des points d\'intérêt  de la collection ' + req.params.nomCollection);
    var i;
        UtilisateurModel.findOne({'nom':req.params.nom}, function(err, utilisateur){
            if(err) throw err;
            CollectionModel.findOne({'nom':req.params.nomCollection}, function(err, col){
                if (err) throw err;

                if(col!=null){
                    for(i=0; i<utilisateur.lesCollections.length; i++)
                    {
                        if(utilisateur.lesCollections[i].nom == col.nom)
                        {
                            utilisateur.lesCollections[i].lesPointsInteret = [];
                            utilisateur.save();
                            res.status(204).end();
                            break;
                        }
                    }
                }
                else{
                    res.json({message:'La collection n\'existe pas'});
                }

            });
        });
    })

//Route désignant un certain point d'interet
routerApi.route('/utilisateurs/:nom/collections/:nomCollection/pointsinteret/:nomPtInteret')
//modification d'un certain point d'intérêt 
    .put(function (req, res) {
            // Modification.
            var i;
            var j;
            //Le point modifié
            var lePoint = new PointsInteretModel();
            lePoint.nom=req.body.nom;
            lePoint.typePoint=req.body.typePoint;
            lePoint.commentaire=req.body.commentaire;
            lePoint.date=req.body.date;
            lePoint.coordonnees=req.body.coordonnees;

            console.log('Modification du  point d\'intérêt  ' + req.params.nomPtInteret + ' dans la collection ' + req.params.nomCollection);
            UtilisateurModel.findOne({'nom':req.params.nom}, function(err, utilisateur){
                //récupération de la collection dans laquelle insérer le nouveau point d'intérêt
                CollectionModel.findOne({'nom':req.params.nomCollection}, function (err, Collection) {
                if (err) throw err;
                //récupération du vecteur de points d'intérêt
                for(i=0; i < utilisateur.lesCollections.length; i++)
                {
                    if(utilisateur.lesCollections[i]!=null && utilisateur.lesCollections[i].nom==Collection.nom)
                    {
                        var lespts = utilisateur.lesCollections[i].lesPointsInteret;
                        utilisateur.lesCollections.splice(i,1);
                        PointsInteretModel.findOne({'nom':req.params.nomPtInteret}, function(err, pointInteret)
                        {
                            for(j=0; j<lespts.length; j++)
                            {
                                if(lespts[j].nom==pointInteret.nom)
                               {
                                   var laCollection = new CollectionModel();
                                    laCollection.nom=Collection.nom;
                                    laCollection.lesPointsInteret=Collection.lesPointsInteret;
                                    laCollection.lesPointsInteret.push(lePoint);
                                    utilisateur.lesCollections.push(laCollection);
                                    utilisateur.save(function (err) {                    
                                    if (err) throw err;
                                        });

                                    res.status(200).json(lePoint);
                                    break;
                               }
                           }
                        });
                    }

                    if(utilisateur.lesCollections[i]==null)
                    {

                    }
                }
            });
        });
    })

// Consultation d'un certain point d'intérêt 
.get(function (req, res) {
    console.log('Consultation du point d\'intérêt ' + req.params.nomPtInteret);

    var i;
    var j;
    UtilisateurModel.findOne({'nom':req.params.nom}, function(err, util)
    {
        if(err) throw err;
        CollectionModel.findOne({'nom':req.params.nomCollection}, function(err, col)
        {
            if(err) throw err;
            if(col!=null || col.nom!='')
            {
                for(i=0; i < util.lesCollections.length; i++)
                {
                    if(util.lesCollections[i].nom==col.nom)
                    {
                        for(j=0; j<util.lesCollections[i].lesPointsInteret.length; j++)
                        {
                            if(util.lesCollections[i].lesPointsInteret[j].nom==req.params.nomPtInteret)
                            {
                                res.status(200).json(util.lesCollections[i].lesPointsInteret[j]);
                                break;
                            }
                        }
                    }
                }
                if(i==util.lesCollections.length)
                {
                    res.status(204).end();
                }
            }
            else{
                res.status(204).end();
            }
        });
    });
})

// Destruction d'un certain point d'intérêt.
.delete(function (req, res) {
    console.log('Destruction du point d\'intérêt  no. ' + req.params.nomPtInteret);
    var i;
    var j;
        UtilisateurModel.findOne({'nom':req.params.nom}, function(err, utilisateur){
            if(err) throw err;
            CollectionModel.findOne({'nom':req.params.nomCollection}, function(err, col){
                if (err) throw err;

                if(col!=null){
                    for(i=0; i < utilisateur.lesCollections.length; i++)
                    {
                        if(utilisateur.lesCollections[i].nom==col.nom)
                        {
                            for(j=0; j<utilisateur.lesCollections[i].lesPointsInteret.length; j++)
                            {
                                PointsInteretModel.findByIdAndRemove(utilisateur.lesCollections[i].lesPointsInteret[j]._id, function(err)
                                {
                                    if(err) throw err;
                                });
                                utilisateur.lesCollections[i].lesPointsInteret.splice(j,1);
                                utilisateur.save();
                                res.status(204).end();
                                break;
                            }
                        }
                    }
                }
                else{
                    res.json({message:'La collection n\'existe pas'});
                }

            });
        });
})


// Méthode HTTP non permise
.all(function (req, res) {
console.log('Méthode HTTP non permise.');
res.status(405).end();
});

// Racine de l'API.
routerApi.get('/', function (req, res) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain; charset=utf-8');
    res.end('L\'API fonctionne bien !');
});
// Rendre l'objet router disponible de l'extérieur.
module.exports = routerApi;
