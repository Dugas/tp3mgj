//import { CollectionSchema } from './collectionModel';

// ORM Mongoose.
var mongoose = require('mongoose');
// Classe pour les schéma Mongoose.
var Schema = mongoose.Schema;

var PointsInteretSchema = new Schema({
    nom : String,
    commentaire: String,
    date: Date,
    typePoint: String,
    coordonnees : [{
        lat : String,
        lng : String
            }]
});

var CollectionSchema = new Schema({
    nom: String,
    lesPointsInteret: [PointsInteretSchema]
});

var UtilisateurSchema = new Schema({
    nom: String,
    passe: String,
    courriel: String,
    lesCollections: [CollectionSchema]
});


// Rendre la classe Utilisateur disponible de l'extérieur.
module.exports.utilisateurModel = mongoose.model('Utilisateur', UtilisateurSchema);
module.exports.pointsInteretModel = mongoose.model('PointsInteret', PointsInteretSchema);
module.exports.collectionModel = mongoose.model('Collection', CollectionSchema);